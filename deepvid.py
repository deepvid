#!/bin/env python
"""
Deepvid Copyright Mikolaj 'lich' Halber 2016
"""
from urllib import request, error
import random
import string
import os
import json
import sqlite3 as sdb
from sys import argv

def baseinit(basefile='deepvid.db'):
    conn = sdb.connect(basefile)
    curr = conn.cursor()
    curr.execute("CREATE TABLE IF NOT EXISTS Videos(ID TEXT, Title TEXT, Views INT, Link TEXT)")

def baseadd(data, basefile='deepvid.db'):
    conn = sdb.connect(basefile)
    curr=conn.cursor()
    data.append('https://youtu.be/' + data[0])
    curr.execute("INSERT INTO Videos VALUES (?, ?, ?, ?)", tuple(data))
    conn.commit()

def random_char(length):
    """Pretty simple function for getting random strings"""
    return ''.join(random.choice(string.ascii_letters) for x in range(length))

def links(key_loc, iterations, no_next_page):
    """Gets links for videos under 20 views"""
    old_queries = []
    good_vids = []
    baseinit()
    with open(key_loc, 'r') as myfile:
        key = myfile.read().replace('\n', '')
    next_page = 0
    data = ""
    for just_dont_use_this_var in range(0, iterations):
        #does the search $iterations number of times
        if not next_page or no_next_page:
            #check if next page was chosen
            query = random_char(4)
            nxt_pg_tok = ""
            if query in old_queries:
                continue
            else:
                old_queries.append(query)
        else:
            nxt_pg_tok = "&pageToken={}".format(data["nextPageToken"])
        link = "https://www.googleapis.com/youtube/v3/search" \
               "?part=snippet&type=video&q={}&key={}&order=date&maxResults=50{}".format( \
                query, key, nxt_pg_tok)
        try:
            req = request.urlopen(link).read().decode('utf-8')
            data = json.loads(req)
        except error.HTTPError:
            continue
        if "nextPageToken" in data:
            next_page = 1 #check for next page token
        else:
            next_page = 0
        if data["pageInfo"]["totalResults"] == 0:
            continue
        for i in data["items"]:
            if i["id"]["videoId"] in good_vids:
                continue #throw away videos that were already found
            v_link = "https://www.googleapis.com/youtube/v3/videos" \
                     "?part=statistics&id={}&key={}".format(i["id"]["videoId"], key)
            v_req = request.urlopen(v_link).read().decode('utf-8')
            v_data = json.loads(v_req)
            if int(v_data["items"][0]["statistics"]["viewCount"]) <= 20:
                good_vids.append(v_data["items"][0]["id"])
                baseadd([i["id"]["videoId"], i["snippet"]["title"], \
                        int(v_data["items"][0]["statistics"]["viewCount"])])
            else:
                continue
    return 0

def main():
    """Main function"""
    print(links(argv[1], 150, True))
if __name__ == '__main__':
    main()
